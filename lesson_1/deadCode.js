const [name, setName] = useState('Dave')
const handleNameChange = () => {
  const names = ['Cape', 'Cooper', 'Cassi', 'Justin'];
  const int = Math.floor(Math.random() * 4);
  setName(names[int]);
}

const handleClick = () => {
  console.log('You clicked it')
}

const handleClick3 = (e) => {
  console.log(e.target.innerText)
}

const handleClick2 = (name) => {
  console.log(`${name} was clicked`)
}


// our default state for the grocery list, we don't need a default state anymore if we want to update it

const [items, setItems] = useState([
  {
    id: 1,
    checked: false,
    item: "One half pound bag of Almonds"
  },
  {
    id: 2,
    checked: false,
    item: "Another half pound bag of Almonds"
  },
  {
    id: 3,
    checked: false,
    item: "Yet another half pound bag of Almonds"
  }
]);
