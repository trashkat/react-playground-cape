import LineItem from "./LineItem"

const ItemList = ({ items, handleCheck, handleDelete }) => {
  return (
    <ul>
    {items.map((item) => (
      <LineItem
        key={item.id}
        item={item}    // don't need to put items={items} because the map function is defining it, but it's not defining the singular "item"
        handleCheck={handleCheck}
        handleDelete={handleDelete}
      />
    ))}
  </ul>
  )
}

export default ItemList
