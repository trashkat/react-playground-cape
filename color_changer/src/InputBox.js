

const InputBox = ({ color, setColor }) => {
  return (
    <form className='inputBox' onSubmit={(e) => e.preventDefault}>
      <label htmlFor='inputColor'>Pick Color</label>
      <input
        id='inputColor'
        type='text'
        role='searchbox'
        placeholder='color lookup'
        value={color}
        onChange={(e) => setColor(e.target.value)}
      />
    </form>
  )
}

export default InputBox
