

const ColorBox = ({ color }) => {
  return (
    <section className="colorBox" style={{backgroundColor: `${color}`}}>
      <p>
        { color }
      </p>
    </section>
  )
}

export default ColorBox
